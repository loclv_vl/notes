# workflow

workflow khi trao đổi với *Khách hàng/PM* - ký hiệu là `K`

có thể áp dụng cho *BrSE, technical leader, team leader* - ký hiệu là `B`

phần mềm quản lý task thông thường như:
- backlog
- jira
- trello
- ...

```sequence
B->K: Says Hello
Note right of K: K thinks\nabout it
K->B: How are you?
B->>K: I am good thanks!
```

# How to view this document

https://medium.com/technical-writing-is-easy/diagrams-in-documentation-markdown-guide-4e78419e8d2f

https://shd101wyy.github.io/markdown-preview-enhanced/#/
